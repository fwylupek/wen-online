#!/usr/bin/python3

import sys
import os
import json


# Replace tone number with diacritics (accents).
def replace_tone(pinyin):
    working_list = []

    a_set = ('ā', 'á', 'ă', 'à')
    e_set = ('ē', 'é', 'ĕ', 'è')
    i_set = ('ī', 'í', 'ĭ', 'ì')
    o_set = ('ō', 'ó', 'ŏ', 'ò')
    u_set = ('ū', 'ú', 'ŭ', 'ù')
    v_set = ('ǖ', 'ǘ', 'ǚ', 'ǜ')

    for word in pinyin:
        for num in range(4):
            word = word.replace(str('a' + str(num + 1)), a_set[num])
            word = word.replace(str('e' + str(num + 1)), e_set[num])
            word = word.replace(str('i' + str(num + 1)), i_set[num])
            word = word.replace(str('o' + str(num + 1)), o_set[num])
            word = word.replace(str('u' + str(num + 1)), u_set[num])
            word = word.replace(str('v' + str(num + 1)), v_set[num])
        working_list.append(word)

    return working_list


# Load a set of words from the "dictionaries/" directory into lists.
def load_file(file_name, characters, pinyin, translation):
    file_name = 'dictionaries/' + file_name.strip()
    
    if os.path.exists(file_name):
        open_file = open(file_name, 'r', encoding='utf8')
        
        for line in open_file:
            working_list = list(line.split(', '))
            characters.append(working_list[0].strip())
            pinyin.append(working_list[1].strip())
            translation.append(working_list[2].strip())
        
        open_file.close()
        new_pinyin = replace_tone(pinyin)
        
        for word in range(len(new_pinyin)):
            pinyin[word] = new_pinyin[word]

        load_status = True
    
    else:
        print('File not found.')
        
        load_status = False

    return load_status


def json_convert(file_name, characters, pinyin, translation):
    working_list = []
    for line in range(len(characters)):
        working_line = {'characters':characters[line],
                                   'pinyin':pinyin[line],
                                   'translation':translation[line]}
        working_list.append(working_line)

    open_file = open('json_objects/' + file_name + '.json', 
                     'w', encoding='utf8')
    open_file.write(json.dumps(working_list, indent=4,
                    ensure_ascii=False))
    open_file.close()


def main():
    argument_number = len(sys.argv)
    if argument_number < 2:
        file_name = input('Filename: ')
        characters = []
        pinyin = []
        translation = []

        if load_file(file_name, characters, pinyin, translation):
            pinyin = replace_tone(pinyin)
            json_convert(file_name, characters, pinyin, translation)
    else:
        while argument_number > 1:
            file_name = sys.argv[argument_number - 1]
            characters = []
            pinyin = []
            translation = []

            if load_file(file_name, characters, pinyin, translation):
                pinyin = replace_tone(pinyin)
                json_convert(file_name, characters, pinyin, translation)
            argument_number -= 1


main()

