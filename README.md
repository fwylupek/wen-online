# Wen Online - A Web Page for Practicing Chinese Characters

Wen generates a quiz for Mandarin Chinese characters by reading JSON data from
the "json_objects/" directory in the working directory. The quiz shows a 
character, with or without pinyin with diacritics, and gives the user a text
box and a submit button to define it. It is written entirely in vanilla
JavaScript, HTML, and CSS.

## Creating Dictionaries

Wen Online uses JSON data to generate quizzes. The data can be created from
comma-separated text files with the "dump_json.py" Python 3 script in the
working directory.

To use "dump_json.py" to generate a custom test, the file must contain one
translation per line. Each line has three parts, separated by a comma (", "):

    1. The Chinese character(s) (e.g. "汉字")
    2. The Hanyu Pinyin with tone numbers (e.g. "Ha4nzi4")
    3. The definition, or definitions separated by a forward slash ("/") 
       (e.g. "Chinese character" or "Chinese chracter/written character")
