var response_data;
var question_number = 0;
var correct_answers = 0;
var user_answers = [];
var user_input = '';
var with_pinyin = 'yes';
var loadingID;
var timeoutID;

// Load the questions from JSON for a quiz.
function load_data() {
    var json_request = new XMLHttpRequest();
    json_request.open('GET','./json_objects/'
        + document.getElementById('select_subject').value + '.json');
    json_request.onload = function() {
        response_data = JSON.parse(json_request.responseText);
    }
    json_request.send();
    // Start test after 300 milliseconds to ensure JSON has loaded.
    loadingID = window.setTimeout(start_test, 300);
    with_pinyin = document.getElementById('select_pinyin').value;
}

// Load the questions from JSON to display in a table.
function load_review() {
    var json_request = new XMLHttpRequest();
    json_request.open('GET','./json_objects/'
        + document.getElementById('select_subject').value + '.json');
    json_request.onload = function() {
        response_data = JSON.parse(json_request.responseText);
    }
    json_request.send();
    // Start test after 300 milliseconds to ensure JSON has loaded.
    loadingID = window.setTimeout(show_review, 300);
    document.getElementById('body').innerHTML += '<br/><br/>';
    document.getElementById('input_text').hidden = true;
    document.getElementById('user_submit').hidden = true;
    document.getElementById('show_pinyin').hidden = true;
    document.getElementById('character').hidden = true;
    document.getElementById('refresh_button').hidden = false;
}

function randomize_questions(original_array) {
    var current_index = original_array.length,
                        temporary_value,
                        random_index;
    while (0 !== current_index) {
        random_index = Math.floor(Math.random() * current_index);
        current_index--;
        temporary_value = original_array[current_index];
        original_array[current_index] = original_array[random_index];
        original_array[random_index] = temporary_value;
    }
    return original_array;
}

// Prepare the page for the test.
function start_test() {
    document.getElementById('character').style.fontSize = 'xx-large';
    response_data = randomize_questions(response_data);
    document.getElementById('input_text').hidden = false;
    document.getElementById('user_submit').hidden = false;
    document.getElementById('input_text').value = '';
    document.getElementById('header').innerHTML =
        'Question ' + (question_number + 1);
    show_character();
}

// Replace header with question number and display character for
// question. Hide text box and submit button at the end of the test.
function show_character() {
    if (question_number <= response_data.length - 1) {
        document.getElementById('header').innerHTML =
            'Question ' + (question_number + 1);
        if (with_pinyin == 'yes') {
            document.getElementById('character').innerHTML =
                response_data[question_number].characters
                + '<br/>' + response_data[question_number].pinyin;
        } else {
            document.getElementById('character').innerHTML =
                response_data[question_number].characters;
            document.getElementById('show_pinyin').hidden = false;
        }
    } else {
        document.getElementById('input_text').hidden = true;
        document.getElementById('user_submit').hidden = true;
        document.getElementById('show_pinyin').hidden = true;
    }
}

// Allows user to submit data on Return/Enter key.
document.getElementById('input_text')
    .addEventListener('keyup', function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById('user_submit').click();
    }
})

// Shows otherwise hidden pinyin as a hint to the user.
function show_hidden_pinyin() {
    document.getElementById('character').innerHTML =
        response_data[question_number].characters
        + '<br/>' + response_data[question_number].pinyin;
}

// Checks user input against known translations and gives feedback.
function get_user_input() {
        user_input = document.getElementById('input_text').value.toLowerCase();
    if (user_input != '') {
        user_answers.push(user_input);
        let evaluation = false;
        // Multiple translations are indicated by a '/'.
        if (response_data[question_number].translation.includes('/')) {
            let multiple_answers =
                response_data[question_number].translation.split('/');
            for (var i = 0; i < multiple_answers.length; i++) {
                if (user_input.trim() == multiple_answers[i].toLowerCase()) {
                    evaluation = true;
                }
            }
        }
        if (user_input.trim() ==
            response_data[question_number].translation.toLowerCase()) {
            evaluation = true;
        }
        if (evaluation) {
            document.getElementById('feedback').innerHTML = 'Correct!';
            correct_answers++;
        } else {
            document.getElementById('feedback').innerHTML =
                'Sorry, the ' + 'correct answer is '
                + response_data[question_number].translation
                + '.';
        }
        iterate_question();
        show_character();
    }
    document.getElementById('input_text').value = '';
}

// Update question number on page and in script, and clear feedback
// after a little over a second has passed.
function iterate_question() {
    if (question_number <= response_data.length) {
        document.getElementById('header').innerHTML =
            'Question ' + (question_number + 1);
        question_number++;
    }
    timeoutID = window.setTimeout(clear_feedback, 1150);
}

// Clear feedback on previous question, and on completion show score
// and default header, clear character from question before and show
// refresh button.
function clear_feedback() {
    window.clearTimeout(clear_feedback);
    document.getElementById('feedback').innerHTML = '';
    if (question_number >= response_data.length) {
        document.getElementById('feedback').innerHTML +=
            '<br/>Score: '
            + ((correct_answers/response_data.length) * 100).toFixed(2) + '%';
        document.getElementById('character').hidden = true;
        document.getElementById('header').innerHTML =
            'Wen: Chinese Character Practice';
        document.getElementById('character').innerHTML = '';
        document.getElementById('refresh_button').hidden = false;
        if (question_number == response_data.length) {
            show_results();
            question_number += 1;
        }
    }
}

// Display a table with as many columns as questions, and questions,
// user submissions, and correct answers as rows.
function show_results() {
    document.getElementById('body').innerHTML += '<br/><br/>';
    let body = document.getElementById('body');
    let new_table = document.createElement('results_table');
    let table_body = document.createElement('tbody');
    let table_header = document.createElement('thead');
    let table_header_row = document.createElement('tr');
    table_header_row.setAttribute("style", "color: white; background-color:black; font-weight: bold;");

    let heading_cell_1 = document.createElement('td');
    let heading_text_1 = document.createTextNode('Question');
    heading_cell_1.appendChild(heading_text_1);
    table_header_row.appendChild(heading_cell_1);

    let heading_cell_2 = document.createElement('td');
    let heading_text_2 = document.createTextNode('Your answer');
    heading_cell_2.appendChild(heading_text_2);
    table_header_row.appendChild(heading_cell_2);

    let heading_cell_3 = document.createElement('td');
    let heading_text_3 = document.createTextNode('Correct answer');
    heading_cell_3.appendChild(heading_text_3);
    table_header_row.appendChild(heading_cell_3);
    table_header.appendChild(table_header_row);
    new_table.appendChild(table_header);

    for (let i = 0; i < response_data.length; i++) {
        var table_row = document.createElement('tr');
        var evaluation = false;
        if (response_data[i].translation.includes('/')) {
                let multiple_answers = response_data[i].translation.split('/');
                for (var j = 0; j < multiple_answers.length; j++) {
                    if (user_answers[i].trim() == multiple_answers[j].toLowerCase()) {
                        evaluation = true;
                    }
                }
            }
        else {
            if (user_answers[i].trim() == response_data[i].translation.toLowerCase()) {
                    evaluation = true;
                    }
        }
        if (evaluation == false) {
            table_row.setAttribute("style", "background-color:#d3d3d3;");
        }
        var table_data = document.createElement('td');
        table_data.appendChild(document.createTextNode(
            response_data[i].characters +
            ' ' + response_data[i].pinyin));
        table_row.appendChild(table_data);
        table_data = document.createElement('td');
        table_data.appendChild(document.createTextNode(
            user_answers[i]));
        table_row.appendChild(table_data);
        table_data = document.createElement('td');
        table_data.appendChild(document.createTextNode(
            response_data[i].translation));
        table_row.appendChild(table_data);
        table_body.appendChild(table_row);
    }
    new_table.appendChild(table_body);
    body.appendChild(new_table);
}

// Display a table with all words in section for review.
function show_review() {
    let body = document.getElementById('body');
    let new_table = document.createElement('review_table');
    let table_body = document.createElement('tbody');
    let table_header = document.createElement('thead');
    let table_header_row = document.createElement('tr');
    table_header_row.setAttribute("style", "color: white; background-color:black; font-weight: bold;");

    let heading_cell_1 = document.createElement('td');
    let heading_text_1 = document.createTextNode('Question');
    heading_cell_1.appendChild(heading_text_1);
    table_header_row.appendChild(heading_cell_1);

    let heading_cell_2 = document.createElement('td');
    let heading_text_2 = document.createTextNode('Correct answer');
    heading_cell_2.appendChild(heading_text_2);
    table_header_row.appendChild(heading_cell_2);
    table_header.appendChild(table_header_row);
    new_table.appendChild(table_header);

    for (let i = 0; i < response_data.length; i++) {
        var table_row = document.createElement('tr');
        var table_data = document.createElement('td');
        table_data.appendChild(document.createTextNode(
            response_data[i].characters +
            ' ' + response_data[i].pinyin));
        table_row.appendChild(table_data);
        table_data = document.createElement('td');
        table_data.appendChild(document.createTextNode(
            response_data[i].translation));
        table_row.appendChild(table_data);
        table_body.appendChild(table_row);
    }
    new_table.appendChild(table_body);
    body.appendChild(new_table);
}
